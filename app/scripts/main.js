(function() {
    $('#slider').slick({
        arrows: false,
        dots: true,
        speed: 500,
        autoplay: true,
        autoplaySpeed: 2000,
    });

    if (location.search) {
        var downloadLink = $('.js-download-link');
        downloadLink.attr({
            'href': downloadLink.attr('href') + location.search
        });
    }

})();
